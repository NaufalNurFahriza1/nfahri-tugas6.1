import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
} from 'react-native';

import Routing from './src/screen/Routing';


const App = () => {

  return (
    <SafeAreaView style={{flex: 1}}>
        <Routing />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
});

export default App;
