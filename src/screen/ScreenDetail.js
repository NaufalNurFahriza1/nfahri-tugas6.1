import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Modal,
  ActivityIndicator,
  ImageBackground,
  ScrollView,
  Dimensions,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import API from '../utils/service/apiProvider';
import { convertCurrency } from '../utils/helpers';
import Carousel from 'react-native-snap-carousel';
import { useIsFocused } from '@react-navigation/native';
export const SLIDER_WIDTH = Dimensions.get('window').width + 10;
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);

const ScreenDetail = ({ navigation, route }) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      console.log('datamobil', data);
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  var dataMobil = route.params;

  //get untuk carosel <

  const [dataMobil2, setDataMobil2] = useState([]);

  const isFocused = useIsFocused();
  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const getDataMobil = async () => {
    const response = await API.getDataMobil();
    console.log('[tugas 61 LOG]: data2', response);
    if (response?.items?.length) {
      setDataMobil2(response.items);
    }
  };


  const CarouselCardItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={{ alignSelf: 'center' }}
        onPress={() => navigation.navigate('ScreenDetail', item)}>
        <View style={styles.carouselcontainer} key={index}>
          <Image source={{ uri: item.unitImage }} style={styles.carouselimage} />
          <Text style={styles.carouselheader}>{item.title}</Text>
          <View style={{ flexDirection: 'row', alignContent: 'space-between' }}>
            <Text style={styles.carouselbody}>
              {convertCurrency(item.harga, 'Rp. ')}
            </Text>
            <Text style={styles.carouselbody}>
              {item.totalKM} KM
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const isCarousel = React.useRef(null);
  //get untuk carosel />

  const editData = async () => {
    setLoading(true);
    if (!namaMobil || !totalKM || !hargaMobil) {
      setLoading(false);
      alert('Nama mobil, Total Kilometer dan Harga Mobil tidak boleh');
      return;
    }

    if (hargaMobil < 20000000) {
      setLoading(false);
      alert(
        'Harga mobil tidak boleh kosong dan tidak boleh dibawah 20 juta rupiah',
      );
      return;
    }

    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://carmudi-journal.icarcdn.com/carmudi-id/wp-content/uploads/2021/08/09222848/Harga-Toyota-Fortuner-GR-Sport.jpg',
      },
    ];

    const response = await API.editData(body);
    console.log('[Admin LOG]: edit', response);
    navigation.navigate('Home');
  };

  const deleteData = async () => {
    setLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];

    const response = await API.deleteData(body);
    console.log('[Admin LOG]: delete', response);

    if (response?.items?.length) {
      navigation.navigate('Home');
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 10 }}>
        <Modal animationType="fade" transparent={true} visible={loading}>
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,0.2)',
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 20,
                borderRadius: 18,
                backgroundColor: '#FFFFFF',
              }}>
              <ActivityIndicator size="large" color="#00FF00" />
            </View>
          </View>
        </Modal>

        <View style={{ width: '100%' }}>
          <ImageBackground
            // source={require('../assets/image_seturan.png')} //load asset dari local
            source={{ uri: dataMobil.unitImage }}
            style={{
              width: '100%',
              height: 316,
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                }}>
                <Icon name="arrowleft" size={20} color="#fff" />
              </TouchableOpacity>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#fff' }}>
                Screen Detail
              </Text>
            </View>
          </ImageBackground>
        </View>
        <View
          style={{
            paddingHorizontal: 20,
            paddingBottom: 14,
            paddingTop: 24,
            marginTop: -30,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: '#FFFFFF',
          }}></View>

        <View
          style={{
            width: '100%',
            padding: 15,
          }}>
          <View>
            <Text style={{ fontSize: 18, fontWeight: '700', color: '#201F26' }}>
              Detail Mobil
            </Text>
            <Text
              style={{
                color: '#595959',
                marginVertical: 10,
                fontSize: 15,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            <Text
              style={{
                fontSize: 16,
                color: '#000',
                fontWeight: '600',
                paddingRight: 5,
              }}>
              Nama Mobil :
            </Text>
            <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
              {dataMobil.title}
            </Text>
          </View>

          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            <Text
              style={{
                fontSize: 16,
                color: '#000',
                fontWeight: '600',
                paddingRight: 5,
              }}>
              Total KM :
            </Text>
            <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
              {dataMobil.totalKM}
            </Text>
          </View>

          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            <Text
              style={{
                fontSize: 16,
                color: '#000',
                fontWeight: '600',
                paddingRight: 5,
              }}>
              Harga Mobil :
            </Text>
            <Text style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
              {convertCurrency(dataMobil.harga, 'Rp. ')}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => setModalVisible(true)}
            style={styles.btnEdit}>
            <Text style={{ color: '#fff', fontWeight: '600' }}>Edit Data</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={deleteData} style={styles.btnDel}>
            <Text style={{ color: '#fff', fontWeight: '600' }}>Hapus Data</Text>
          </TouchableOpacity>

          <Carousel
            containerCustomStyle={{ alignSelf: 'center' }}
            layout="default"
            layoutCardOffset={5}
            ref={isCarousel}
            data={dataMobil2.slice(0, 10)}
            renderItem={CarouselCardItem}
            sliderWidth={300}
            itemWidth={300}
            inactiveSlideShift={0}
            useScrollView={true}
          />
        </View>


        <Modal animationType="fade" transparent={true} visible={modalVisible}>
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,0.2)',
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                paddingHorizontal: 10,
                paddingVertical: 10,
                borderRadius: 18,
                backgroundColor: '#FFFFFF',
                width: '85%',
                height: '65%',
                alignContent: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => setModalVisible(false)}
                style={{
                  width: '10%',
                  alignSelf: 'flex-end',
                }}>
                <Icon name="close" size={20} color="#000" />
              </TouchableOpacity>

              <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#000' }}>
                  Edit Data
                </Text>
              </View>

              <View
                style={{
                  width: '100%',
                  padding: 5,
                }}>
                <View>
                  <Text
                    style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Nama Mobil"
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    style={styles.txtInput}
                  />
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    placeholder="contoh: 100 KM"
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    style={styles.txtInput}
                  />
                </View>

                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{ fontSize: 16, color: '#000', fontWeight: '600' }}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    placeholder="Masukkan Harga Mobil"
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>

                <TouchableOpacity onPress={editData} style={styles.btnEdit}>
                  <Text style={{ color: '#fff', fontWeight: '600' }}>
                    Edit Data
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  btnEdit: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#00A0F0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnDel: {
    marginTop: 20,
    marginBottom: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#EE5F85',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
  carouselcontainer: {
    backgroundColor: 'white',
    borderRadius: 8,
    borderColor: '#dedede',
    borderWidth: 1,
    width: 300,
    paddingBottom: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 8,
    elevation: 4,
    marginVertical: 10,
  },
  carouselimage: {
    alignSelf: 'center',
    marginTop: 10,
    width: '95%',
    // width: Dimensions.get('screen').width,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#dedede',

    height: 150,
  },
  carouselheader: {
    color: '#222',
    fontSize: 24,
    fontWeight: 'bold',
    paddingLeft: 15,
    paddingTop: 15,
  },
  carouselbody: {
    color: '#222',
    fontSize: 14,
    paddingLeft: 15,
    paddingRight: 15,
  },
});

export default ScreenDetail;
