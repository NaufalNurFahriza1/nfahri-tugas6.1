import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";

import Home from "./Home";
import ScreenDetail from "./ScreenDetail";
// import Carousel from "react-native-snap-carousel";

const Stack = createNativeStackNavigator();

export default function Routing() {
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="ScreenDetail" component={ScreenDetail} />
                {/* <Stack.Screen name="Carousel" component={Carousel} /> */}
            </Stack.Navigator>
        </NavigationContainer>
    )
}