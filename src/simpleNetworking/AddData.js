import React, {useEffect ,useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Modal,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';

const AddData = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [loading, setLoading] = useState(false);

  const postData = async () => {
    setLoading(true)

    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://gitlab.com/uploads/-/system/user/avatar/3683300/avatar.png?width=400',
      },
    ];

    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      console.log('Success:', result);
      Alert.alert('Alert Title', 'Data Mobil berhasil ditambahkan', [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => navigation.goBack()},
        
      ])
    } catch (error) {
      console.error('Error:', error);
    }
    
  setLoading(false)
  setTimeout(()=> {
  }, 2000)
  };

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={loading}
        >
        <View style={{
          width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.2)', alignContent:'center',
          justifyContent: 'center', alignItems: 'center'
        }}>
        <View style={{
          padding:20, borderRadius: 18, backgroundColor: '#FFFFFF'
        }}>
        <ActivityIndicator size="large" color="#00FF00" />
        </View>
      </View>
      </Modal>

      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          Tambah Data
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Nama Mobil"
            value={namaMobil}
            onChangeText={text => setNamaMobil(text)}
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            placeholder="contoh: 100 KM"
            value={totalKM}
            onChangeText={text => setTotalKM(text)}
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Harga Mobil"
            value={hargaMobil}
            onChangeText={text => setHargaMobil(text)}
            style={styles.txtInput}
            keyboardType="number-pad"
          />
        </View>
        <TouchableOpacity onPress={postData} style={styles.btnAdd}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Tambah Data</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
