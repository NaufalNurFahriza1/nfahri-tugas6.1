import axios from "axios";
import { BASE_URL, TOKEN } from './url';

const API = async (
    url,
    options = {
        method: 'GET',
        body: {},
        headers: {}
    },
) => {
    const request = {
        baseURL: BASE_URL,
        method: options.method,
        timeout: 10000,
        url,
        headers: options.headers,
        responseType: 'json',
    }
    console.log('1234 request', request);
    if (request.method === 'POST' || request.method === 'PUT' || request.method === 'DELETE') request.data = options.body

    const res = await axios(request);
    console.log('1234 res', res);

    if (res.status === 200) {
        return res.data;
    } else {
        return res
    }
};

export default {
    getDataMobil: async () => {
        return API('mobil', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': TOKEN,
            },
        })
            .then((response) => { return response })
            .then((err) => { return err })
    },

    postData: async params => {
        return API('mobil', {
            method: 'POST',
            body: params,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': TOKEN,
            },
        })
            .then((response) => {
                console.log('tambah', response)
                alert('Data Mobil berhasil ditambahkan');
                return response;
            })
            .catch((err) => {
                return err
            });
    },

    editData: async params => {
        return API('mobil', {
            method: 'PUT',
            body: params,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': TOKEN,
            },
        })
            .then((response) => {
                console.log('edit', response)
                alert('Data Mobil berhasil ditambahkan');
                return response;
            })
            .catch((err) => {
                return err
            });
    },

    deleteData: async params => {
        return API('mobil', {
            method: 'DELETE',
            body: params,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': TOKEN,
            },
        })
            .then((response) => {
                console.log('delete', response)
                alert('Data Mobil berhasil dihapus');
                return response;
            })
            .catch((err) => {
                return err
            });
    },
}



